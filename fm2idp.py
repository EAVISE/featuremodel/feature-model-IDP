"""
Convert Feature Models (in json format) to IDP's FO(.) using the semantics as
described by Schobbens et al. (2005).

Author: Simon Vandevelde <s.vandevelde@kuleuven.be>
"""
import argparse
import json


def json_to_IDP(json_data):
    """ Convert json data into FO(.) format """

    nodes = [x for x in json_data.keys() if not x.startswith('_')]
    voc = f"vocabulary {{ {', '.join(nodes)}: () -> Bool}}"
    theory = "theory {\n"
    display = "display {\n"
    for node_name, node in json_data.items():
        if node_name.startswith('_'):
            continue
        # There is a necessary condition for each child node: a child cannot
        # exist without its parent.
        for child, child_relation in node['Children'].items():

            # If the relation is MANdatory, the child and parent imply each
            # other.
            # Else, in the case of OPTional or if the relation is ALTernative
            # or OR, the child implies the parent.
            if child_relation == 'MAN':
                theory += f'\t{node_name}() <=> {child}().\n'
            else:
                theory += f'\t{node_name}() <= {child}().\n'

        # If the relation is OR, at least one of the children should be
        # present.
        # If the relation is ALTernative, exactly one child may be present.
        # If the relation is AND, nothing needs to be added.
        if node['Relation'] == 'OR':
            children = [f'{x}()' for x in node['Children'].keys()]
            theory += f'\t{node_name}() => {" | ".join(children)}.\n'
        elif node['Relation'] == 'ALT':
            children = [f'{x}()' for x in node['Children'].keys()]
            for i in range(len(children)):
                child = children[i]
                other_children = children[:i] + children[i+1:]
                constraint = f'{node_name}() => ({child} <=> ~({" | ".join(other_children)})).'
                theory += f'\t{constraint}\n'
        else:
            pass

        # We want to group the children under the same header.
        if node['Children'].items():
            child_names = ['`' + child for child in node['Children'].keys()]
            display += (f"\theading('{node_name} children',"
                        f"{','.join(child_names)}).\n")
    theory += "}\n"
    display += "}\n"
    return voc + '\n' + theory + display


def main():
    parser = argparse.ArgumentParser(description='Convert FM to IDP')
    parser.add_argument('json', type=str,
                        help='the source json file')
    parser.add_argument('output', type=str,
                        help='the target idp file')
    args = parser.parse_args()

    with open(args.json, 'r') as fp:
        data = json.load(fp)

    idp = json_to_IDP(data)
    with open(args.output, 'w') as fp:
        fp.write(idp)


if __name__ == "__main__":
    main()
