import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {MessageService} from 'primeng/api';
import {DOCUMENT} from '@angular/common';
import {IdpService} from '../../../services/idp.service';
import {Inject} from '@angular/core';

@Component({
  selector: 'app-feature-model-drawer',
  templateUrl: './feature-model-drawer.component.html',
  styleUrls: ['./feature-model-drawer.component.css']
})

export class FeatureModelDrawerComponent implements OnInit {
  @Output() messageEvent: EventEmitter<string> = new EventEmitter<string>() ;
  constructor(public idpService: IdpService) {
    this.idpService = idpService;
  }
  ngOnInit() {
    const idpService = this.idpService;
    const all_boxes = [];
    const floor = Math.floor.bind(Math);
    const id = document.getElementById.bind(document);
    const mEvent = this.messageEvent;
    const svg = id('model')
    function create_box(description, x, y) {
        const box = create_box_element(description, x, y, false, CHILD_NORMAL, PARENT_NORMAL);
        // Set all listeners.
        box.g.onclick = function() {cancel_space_click_function; box_editmode(box);}
        box.g.onmousedown = function(ev) { box_start_move(box, ev); };
        box.g.oncontextmenu = function () {
            // Only toggle a connection if a box is currently selected.
            cancel_space_click = false;
            if ((selected_box == null) || (selected_box == box)) {
                return;
            }
            box_connect_toggle(selected_box, box);
        };
        box_update_complete(box);
        all_boxes.push(box);
        return box;
    }
    function create_box_element (description, x, y, concrete, childstyle, parentstyle) {
        const g = svgel('g', []);              /* Logical box group */
        const t = svgel('text', []);           /* Text field (determines box size) */
        const r = svgel('rect', []);           /* Bordered box */
        const c = svgel('circle', {r: 7});    /* Child type indicator (dot) */
        const p = svgel('path', []);           /* Parent type indicator (arc) */
        t.appendChild(document.createTextNode(description));
        g.appendChild(p);
        g.appendChild(r);
        g.appendChild(t);
        g.appendChild(c);
        svg.appendChild(g);
        const box = { x: x, y: y, g: g, t: t, r: r, c: c, p: p, conns: [],
                     concrete: concrete, childstyle: childstyle, parentstyle: parentstyle };
        return box;
      }
    function svgel (name, attributes) {
        var el = document.createElementNS("http://www.w3.org/2000/svg", name);
        if (attributes != undefined)
              svgatt(el, attributes);
        return el;
      };
    function svgatt (element, attributes) {
        for (var prop in attributes) {element.setAttribute(prop, attributes[prop]);}
      };
      /* SVG image containing one group with all lines and zero or more groups representing boxes */


      const lines = svgel("g",[]);
      svg.appendChild(lines);
      var cancel_space_click = false;
      /* Click events seem to be uncancelable from SVG sub-elements */
      var cancel_space_click_function = function() { cancel_space_click = true; };
      /* Input elements for box editing */
      const settings = id("settings");
      const settings_name = id("boxname");
      const settings_abstract = id("abstract");
      const settings_child = id("child");
      const settings_parent = id("parent");
      const settings_delete = id("delete");
      const CHILD_NORMAL = "C", CHILD_MANDATORY = "M", CHILD_OPTIONAL = "P";
      const PARENT_NORMAL = "A", PARENT_OR = "O", PARENT_ALTERNATIVE = "X";
      /* Box management */


      var pad = { x: 8, y: 6};
      function box_update_complete (box) {
        /* Update text first, then use the bounding box of text to update the background */
        var tbb = box.t.getBBox();
        svgatt(box.t, {x: box.x - floor(tbb.width / 2), y: box.y -floor(tbb.height / 4) });
        tbb = box.t.getBBox();
        svgatt(box.r, {x: tbb.x - pad.x, y: tbb.y - pad.y, width: tbb.width + pad.x * 2, height: tbb.height + pad.y * 2});
        /* Update connected lines */
        box_update_visual(box);
        for (var i in box.conns) {
            var conn = box.conns[i];
            box_update_visual((box == conn.box1) ? conn.box2 : conn.box1);
        }
      };
      var parent_arc_r = 30;
      function box_update_visual (box) {
        var bb = box.r.getBBox();
        var has_parents = false, num_children = 0;
        var min_angle = Math.PI, max_angle = -Math.PI;
        for (var i in box.conns) {
            var conn = box.conns[i];
            var parent_box, child_box, pbb, cbb;
            if (conn.box1.y < conn.box2.y) {
                parent_box = conn.box1, child_box = conn.box2;
            } else {
                parent_box = conn.box2, child_box = conn.box1;
            }
            pbb = parent_box.r.getBBox();
            cbb = child_box.r.getBBox();
            var px = floor(pbb.x + pbb.width / 2), py = pbb.y + pbb.height + 0.5;
            var cx = floor(cbb.x + cbb.width / 2), cy = cbb.y - 0.5;
            svgatt(conn.ln, { x1: px, y1: py, x2: cx, y2: cy });
            if (box == parent_box) {
                var angle = Math.atan2(cx - px, cy - py);
                if (angle < min_angle) min_angle = angle;
                if (angle > max_angle) max_angle = angle;
                ++num_children;
            } else {
                has_parents = true;
            }
        }
        box.r.style.fill = box.concrete ? "#ccc" : null;
        if (box.childstyle != CHILD_NORMAL && has_parents) {
            box.c.style.fill = (box.childstyle == CHILD_MANDATORY) ? "#888" : "#fff";
            svgatt(box.c, {visibility: "visible", cx: floor(bb.x + bb.width / 2), cy: bb.y - 0.5});
        } else {
            svgatt(box.c, {visibility: "hidden"});
        }
        if (box.parentstyle != PARENT_NORMAL && num_children >= 2) {
            box.p.style.fill = (box.parentstyle == PARENT_OR) ? null : "none";
            var sx = floor(bb.x + bb.width / 2), sy = bb.y + bb.height + 0.5;         /* Center bottom */
            var lx = sx + parent_arc_r * Math.sin(max_angle), ly = sy + parent_arc_r * Math.cos(max_angle); /* Left starting point */
            var rx = sx + parent_arc_r * Math.sin(min_angle), ry = sy + parent_arc_r * Math.cos(min_angle); /* Right ending point */
            var draw = "M "+sx+" "+sy+" L "+lx+" "+ly+" A "+parent_arc_r+" "+parent_arc_r+" 0 0 1 "+rx+" "+ry+" Z";
            svgatt(box.p, {visibility: "visible", d: draw});
        } else {
            svgatt(box.p, {visibility: "hidden"});
        }
    };
    /* Connect box_a to box_b
    * box_a is the child, box_b the parent.
    */
    function box_connect_toggle (box_a, box_b) {
      /* Delete from A if they are already connected */
      var removed_connection = false;
      for (var i in box_a.conns) {
          var conn = box_a.conns[i];
          if (conn.box1 == box_b || conn.box2 == box_b) {
              if (conn.ln != null) {
                  lines.removeChild(conn.ln);
                  conn.ln = null;
              }
              box_a.conns.splice(i, 1);
              removed_connection = true;
              break;
          }
      }
      /* Delete from B if they are already connected */
      for (var i in box_b.conns) {
          var conn = box_b.conns[i];
          if (conn.box1 == box_a || conn.box2 == box_a) {
              if (conn.ln != null) {
                  lines.removeChild(conn.ln);
                  conn.ln = null;
              }
              box_b.conns.splice(i, 1);
              removed_connection = true;
              break;
          }
        }
        if (!removed_connection) {
            var ln = svgel("line",[]);
            lines.appendChild(ln);
            var conn1 = { ln: ln, box1: box_a, box2: box_b };
            box_a.conns.push(conn1);
            box_b.conns.push(conn1);
        }
        box_update_visual(box_a);
        box_update_visual(box_b);
    };
    var box_moving = null;
    var box_clickoff = null;
    function box_start_move (box, ev) {
        if (box_moving != null)
            return;
        box_moving = box;
        window.addEventListener("mousemove", box_move_step);
        window.addEventListener("mouseup", box_move_stop);
        cancel_space_click = true;
        /* Offset in the box to avoid jumping of the box */
        box_clickoff = { x: ev.clientX - svg_left - box.x, y: ev.clientY - svg_top - box.y };
    };
    function box_move_step (ev) {
        if (box_moving == null)
            return;
        var x = ev.clientX - svg_left, y = ev.clientY - svg_top;
        box_moving.x = x - box_clickoff.x, box_moving.y = y - box_clickoff.y;
        box_update_complete(box_moving);
    };
    function box_move_stop (ev) {
        window.removeEventListener("mousemove", box_move_step);
        window.removeEventListener("mouseup", box_move_stop);
        box_moving = null;
    };
    function box_delete (box) {
        while (box.conns.length > 0) {
            var conn = box.conns[0];
            box_connect_toggle(conn.box1, conn.box2);
        }
        svg.removeChild(box.g);
        for (var i in all_boxes)
            if (all_boxes[i] == box)
                all_boxes.splice(+i, 1);
    };
    var selected_box = null;
    function box_stop_editmode () {
        settings.style.display = "none";
        settings_name.oninput = null;
        settings_abstract.onchange = null;
        settings_child.onchange = null;
        settings_parent.onchange = null;
        settings_delete.onclick = null;
        selected_box = null;
    };
    function box_editmode (box) {
        if (selected_box != null) {
            selected_box.r.style.stroke = null;
            if (selected_box == box) {
                box_stop_editmode();
                return;
            }
        }
        /* Start edit mode for given box */
        box.r.style.stroke = "#06f";
        selected_box = box;
        settings.style.display = "block"
        settings_name.value = box.t.firstChild.textContent;
        settings_name.oninput = function(ev) {
            box.t.firstChild.textContent = settings_name.value;
            box_update_complete(box);
        };
        settings_abstract.value = box.concrete ? "C" : "A";
        settings_abstract.onchange = function(ev) {
            box.concrete = (settings_abstract.value == "C");
            box_update_visual(box);
        };
        settings_child.value = box.childstyle;
        settings_child.onchange = function(ev) {
            box.childstyle = settings_child.value;
            box_update_visual(box);
        };
        settings_parent.value = box.parentstyle;
        settings_parent.onchange = function(ev) {
            box.parentstyle = settings_parent.value;
            box_update_visual(box);
        };
        settings_delete.onclick = function(ev) {
            box_delete(box);
            box_stop_editmode();
        };
        settings_name.focus();
        settings_name.select();
    };
    /* Click on the empty space to create new boxes */
    function space_clicked (ev) {
        if (cancel_space_click) {
            cancel_space_click = false;
            return;
        }
        var x = ev.clientX - svg_left, y = ev.clientY - svg_top;
        var box = create_box("Box name", x, y + 10);
        box_editmode(box);
    };
    svg.addEventListener("click", space_clicked);

    /* Update SVG size so that it fills the entire screen */
    var svg_top, svg_left;
    function update_svg_pos () {
        var bb = svg.getBoundingClientRect();
        svg_top  = bb.top+8;
        svg_left = 0;
    };
    function fit_screen_size () {
        svg.setAttribute("width", floor(window.innerWidth) + "px");
        svg.setAttribute("height", floor(window.innerHeight) + "px");
        update_svg_pos();
    };
    window.addEventListener("resize", fit_screen_size);
    /* SVG subelements can't cancel default action... need to disable it for entire SVG */
    svg.oncontextmenu = function(ev) { ev.preventDefault(); };
    /* Export options */
    function create_exported_svg () {
        /* Find area with elements */
        var dim = { left: parseInt(svg.getAttribute("width")), right: 0, top: parseInt(svg.getAttribute("height")), bottom: 0 };
        for (var i in all_boxes) {
            var box = all_boxes[i];
            var bb = box.r.getBBox();
            if (bb.x < dim.left)
                dim.left = bb.x;
            if (bb.x + bb.width > dim.right)
                dim.right = bb.x + bb.width;
            if (bb.y < dim.top)
                dim.top = bb.y;
            if (bb.y + bb.height > dim.bottom)
                dim.bottom = bb.y + bb.height;
        }
        var pad_area = 10;
        var translate_x = dim.left - pad_area, translate_y = dim.top - pad_area;
        var new_w = (dim.right - dim.left) + 2 * pad_area, new_h = (dim.bottom - dim.top) + 2 * pad_area;
        /* Create new SVG with appropriate size and copy style */
        var new_svg = svgel("svg", { width: new_w, height: new_h, xmlns: "http://www.w3.org/2000/svg" });
        for (var i in svg.childNodes) {
            if (svg.childNodes[i].tagName == "style") {
                new_svg.appendChild(svg.childNodes[i].cloneNode(true));
                break;
            }
        }
        var new_lines = svgel("g","");
        new_svg.appendChild(new_lines);
        /* Recreate all boxes */
        var translate_boxes = [];
        for (var i in all_boxes) {
            var box = all_boxes[i];
            var new_box = create_box_element(box.t.firstChild.textContent, box.x - translate_x, box.y - translate_y, box.concrete, box.childstyle, box.parentstyle);
            translate_boxes[i] = new_box;
            new_svg.appendChild(new_box.g);
        }
        /* Recreate connections */
        var old_conns = [];
        var new_conns = [];
        for (var i in all_boxes) {
            var box = all_boxes[i];
            var new_box1 = translate_boxes[i];
            for (var j in box.conns) {
                var conn = box.conns[j];
                var oci = old_conns.indexOf(conn);
                if (oci < 0) {
                    old_conns.push(conn);
                    var box2 = (box == conn.box1) ? conn.box2 : conn.box1;
                    var new_box2 = translate_boxes[all_boxes.indexOf(box2)];
                    var ln = svgel("line",[]);
                    new_lines.appendChild(ln);
                    var new_conn = { ln: ln, box1: new_box1, box2: new_box2 };
                    new_conns.push(new_conn);
                    new_box1.conns.push(new_conn);
                } else {
                    new_box1.conns.push(new_conns[oci]);
                }
            }
        }
        /* Insert temporarily to actual DOM, update all new elements, and remove it again */
        document.body.appendChild(new_svg);
        for (var i in all_boxes) {
            box_update_complete(translate_boxes[i]);
        }
        document.body.removeChild(new_svg);
        return { w: new_w, h: new_h, data: URL.createObjectURL(new Blob([new_svg.outerHTML], {type: "image/svg+xml;charset=utf-8"})) };
    };
    /* Export feature diagram to json
     * Each node is described by 4 fields: "Name", "Parent", "Children" and "Relation".
     */
    /*
     * Load a json file into the editor.
     */
    function load_json (json) {
        // First, create every box and set their parentstyle (AND, XOR or OR).
        const boxes = {}
        for (const boxName in json) {
            if (boxName === '_background') {
              continue;
            }
            let box = create_box(boxName, parseInt(json[boxName].x), parseInt(json[boxName].y))

            const parentstyle = json[boxName].Relation
            if (parentstyle === 'AND') {
                box.parentstyle = PARENT_NORMAL;
            } else if (parentstyle === 'XOR') {
                box.parentstyle = PARENT_ALTERNATIVE;
            } else {
                box.parentstyle = PARENT_OR;
            }
            boxes[boxName] = box;
        }

        // Then, create every connection and set the child style (MAN or OPT).
        for (const boxName in json) {
            const children = json[boxName].Children
            for (const childName in children) {
                // Make connection, set the childstyle.
                box_connect_toggle(boxes[childName], boxes[boxName]);
                const childstyle = children[childName];
                if (json[boxName].Relation != 'AND') {
                    boxes[childName].childstyle = CHILD_NORMAL;
                } else if (childstyle === 'OPT') {
                    boxes[childName].childstyle = CHILD_OPTIONAL;
                } else {
                    boxes[childName].childstyle = CHILD_MANDATORY;
                }
            }
        }
        for (const [boxName, box] of Object.entries(boxes)) {
            box_update_visual(box);
        }
        // box_update_visual(box);
        idpService.BKspec = json['_background'];
    }

    id("export_svg").addEventListener("click", function() {
        if (all_boxes.length == 0) {
            alert("Nothing to export.");
            return;
        }
        var expsvg = create_exported_svg();
        var link = document.createElement("a");
        link.href = expsvg.data;
        link.download = "feature_model.svg";
        link.click();
    });
    id("export_png").addEventListener("click", function() {
        if (all_boxes.length == 0) {
            alert("Nothing to export.");
            return;
        }
        var expsvg = create_exported_svg();
        var canv = document.createElement("canvas");
        canv.width = expsvg.w;
        canv.height = expsvg.h;
        var img = new Image();
        img.onload = function () {
            var ctx = canv.getContext("2d");
            ctx.clearRect(0, 0, expsvg.w, expsvg.h);
            ctx.drawImage(img, 0, 0);
            var link = document.createElement("a");
            link.href = canv.toDataURL("image/png");
            link.download = "feature_model.png";
            link.click();
        };
        img.src = expsvg.data;
    });
    id("export_json").addEventListener("click", function () {
        if (all_boxes.length == 0) {
            alert("Nothing to export.");
            return;
        }
        const link = document.createElement("a");
        const json_data = export_json();
        link.href = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json_data));
        link.download = "feature_diagram.json";
        link.click();
    });

    id("load_json").addEventListener("click", function() {
        // Verify if anything has been selected.
        const file = id('upload_file').files[0];
        if (!file) {
            alert('Select a file first.');
            return;
        }
        const fileReader = new FileReader();
        fileReader.onload = function(fileLoadedEvent){
          // @ts-ignore
          const text = fileLoadedEvent.target.result;
          load_json(JSON.parse(text));
        };
        fileReader.readAsText(file, "UTF-8");
    });
    id("reset").addEventListener("click", function() {
        while (all_boxes.length > 0) {
            box_delete(all_boxes[0]);
        }
    });
    /* Let's go !*/
    fit_screen_size();
    box_stop_editmode();
    function send_message(){
      if (all_boxes.length == 0) {
            alert("Nothing to translate.");
            return;
        }
      const json_data = JSON.stringify(export_json());
      mEvent.emit(json_data);
    };
    id("to_idp").addEventListener("click", function () {
      send_message();
    });
    function export_json () {
        const json_data = {};
        json_data['_background'] = idpService.BKspec;
        for (let i in all_boxes) {
            const box_data = {};
            const box = all_boxes[i];
            const name = box.t.textContent;
            const relation = box.parentstyle;
            const children = {};
            let parent_name = '';
            // If the box is listed first in a connection, set the other as parent.
            // If the box is listed second, add the first as child.
            for (let i in box.conns) {
                if (box === box.conns[i]['box1']) {
                    parent_name = box.conns[i]['box2'].t.textContent;
                } else {
                    const child_name = box.conns[i]['box1'].t.textContent;
                    let child_style = box.conns[i]['box1'].childstyle;
                    if (child_style == 'M') {
                        child_style = 'MAN';
                    } else {
                        child_style = 'OPT';
                    }
                    children[child_name] = child_style;
                }
            }
            box_data['Name'] = name;
            box_data['Parent'] = parent_name;
            if (relation === 'A') {
                box_data['Relation'] = 'AND';
            } else if (relation === 'X') {
                box_data['Relation'] = 'ALT';
            } else {
                box_data['Relation'] = 'OR';
            }
            box_data['Children'] = children;
            json_data[name] = box_data;
            box_data['x'] = box.x;
            box_data['y'] = box.y;
        };
        return json_data
    };

    const default_json = {"_background":"vocabulary {\n\n} theory {\n\n}","SealOfzo":{"Name":"SealOfzo","Parent":"","Relation":"AND","Children":{"Nogietsanders":"MAN","Spacer":"OPT"},"x":363,"y":212},"Spacer":{"Name":"Spacer","Parent":"SealOfzo","Relation":"OR","Children":{"Mat1":"OPT","Mat2":"OPT","Mat3":"OPT"},"x":269,"y":330},"Mat1":{"Name":"Mat1","Parent":"Spacer","Relation":"AND","Children":{},"x":145,"y":457},"Mat2":{"Name":"Mat2","Parent":"Spacer","Relation":"AND","Children":{},"x":279,"y":461},"Mat3":{"Name":"Mat3","Parent":"Spacer","Relation":"AND","Children":{},"x":409,"y":467},"Nogietsanders":{"Name":"Nogietsanders","Parent":"SealOfzo","Relation":"AND","Children":{},"x":499,"y":333}}
    load_json(default_json);
  }
}
