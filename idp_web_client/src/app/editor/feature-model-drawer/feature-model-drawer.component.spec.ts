import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureModelDrawerComponent } from './feature-model-drawer.component';

describe('FeatureModelDrawerComponent', () => {
  let component: FeatureModelDrawerComponent;
  let fixture: ComponentFixture<FeatureModelDrawerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureModelDrawerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureModelDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
