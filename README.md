# Feature Model IDP

Feature Model IDP combines an intuitive feature modeling editor with [IDP-Z3](https://www.IDP-Z3.be)'s Interactive Consultant interface.
This way, knowledge can be expressed in easy-to-read feature models, but can be interacted with multi-purposely using IDP-Z3 and a user-friendly interface.
Moreover, it is allows modelers to express additional background knowledge in FO(·).


# Installation

```
git clone https://gitlab.com/EAVISE/featuremodel/feature-model-IDP
cd feature-model-IDP
pip3 install -r requirements.txt
```

# Get started

```
python3 main.py
```

Then open your browser at http://127.0.0.1:5000/IDE


# Acknowledgements

This project depends on 

* The [Interactive Consultant](https://gitlab.com/krr/IDP-Z3)
* [HTML5 feature model diagram editor](https://github.com/evelance/feature_model_drawer), which has been forked to better suit our needs over at [feature-model-editor](https://gitlab.com/EAVISE/featuremodel/feature-model-editor)
